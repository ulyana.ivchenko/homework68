<?php

use Illuminate\Database\Seeder;

class PostTranslationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_translations')->insert(
            ['post_id' => 1,
            'locale' => 'ru',
            'content' => 'Привет'
            ]);
        DB::table('post_translations')->insert(
            ['post_id' => 1,
                'locale' => 'en',
                'content' => 'Hello'
            ]);
        DB::table('post_translations')->insert(
            ['post_id' => 1,
                'locale' => 'fr',
                'content' => 'Bonjour'
            ]);
        DB::table('post_translations')->insert(
            ['post_id' => 1,
                'locale' => 'es',
                'content' => 'Hola'
            ]);
        DB::table('post_translations')->insert(
            ['post_id' => 1,
                'locale' => 'de',
                'content' => 'Hallo'
            ]);

        DB::table('post_translations')->insert(
            ['post_id' => 2,
                'locale' => 'ru',
                'content' => 'Спасибо'
            ]);
        DB::table('post_translations')->insert(
            ['post_id' => 2,
                'locale' => 'en',
                'content' => 'Thank you'
            ]);
        DB::table('post_translations')->insert(
            ['post_id' => 2,
                'locale' => 'fr',
                'content' => ''
            ]);
        DB::table('post_translations')->insert(
            ['post_id' => 2,
                'locale' => 'es',
                'content' => 'Gracias'
            ]);
        DB::table('post_translations')->insert(
            ['post_id' => 2,
                'locale' => 'de',
                'content' => 'Danke'
            ]);

        DB::table('post_translations')->insert(
            ['post_id' => 3,
                'locale' => 'ru',
                'content' => 'До свидания'
            ]);
        DB::table('post_translations')->insert(
            ['post_id' => 3,
                'locale' => 'en',
                'content' => 'Good-bye'
            ]);
        DB::table('post_translations')->insert(
            ['post_id' => 3,
                'locale' => 'fr',
                'content' => 'Au revoir'
            ]);
        DB::table('post_translations')->insert(
            ['post_id' => 3,
                'locale' => 'es',
                'content' => 'Adiós'
            ]);
        DB::table('post_translations')->insert(
            ['post_id' => 3,
                'locale' => 'de',
                'content' => ''
            ]);

    }

}
