<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = Post::all();
        return view('posts.index', compact('posts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PostRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PostRequest $request)
    {
        Auth::user();
        $post = new Post();
        $post->translateOrNew('ru')->content = $request->get('content');
        foreach(['en', 'fr', 'es', 'de'] as $locale) {
            $post->translateOrNew($locale)->content = '';
        }
        $post->user_id = $request->user()->id;
        $post->save();
        return redirect()->route('posts.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Post $post)
    {
        foreach(['en', 'fr', 'es', 'de'] as $locale) {
            if(!$request->get($locale)) {
                $post->translateOrNew($locale)->content = '';
            }
            else {
                $post->translateOrNew($locale)->content = $request->get($locale);
            }
        }
        $post->update();
        return redirect()->route('posts.index');
    }
}
