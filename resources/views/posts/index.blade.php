@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row mb-3">
            <h5>@lang('Введите фразу на русском для перевода'):</h5>
        </div>

        @foreach($posts as $post)
            <div class="row">
                <a href="{{route('posts.edit', ['post' => $post])}}">{{$post->translate('ru')->content}}</a>
            </div>
        @endforeach

        <form method="post" action="{{route('posts.store')}}">
            @csrf
            <div class="form-row mt-5">
                <div class="form-group col-md-8">
                    <label for="content"><b>@lang('Ваша фраза'):</b></label><span class="text-danger">*</span>
                    <textarea class="form-control @error('content') is-invalid @enderror" id="content"
                              name="content"></textarea>
                    @error('content')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-primary">@lang('Добавить')</button>
        </form>
    </div>
@endsection
