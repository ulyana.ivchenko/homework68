@extends('layouts.app')
@section('content')
    <div class="container">
        @if(Auth::user()->id == $post->user_id)
            <div class="row mb-3">
                <h5>@lang('Переводы для') '{{$post->translate('ru')->content}}':</h5>
            </div>
            <div class="row">
                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr class="text-center">
                        <th scope="col">@lang('Английский')</th>
                        <th scope="col">@lang('Французский')</th>
                        <th scope="col">@lang('Испанский')</th>
                        <th scope="col">@lang('Немецкий')</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$post->translate('en')->content}}</td>
                        <td>{{$post->translate('fr')->content}}</td>
                        <td>{{$post->translate('es')->content}}</td>
                        <td>{{$post->translate('de')->content}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row"><a class="pl-4" href="{{route('posts.index')}}">@lang('Назад')</a></div>

        @else
            <div class="row mb-3">
                <h5>@lang('Введите перевод для') '{{$post->translate('ru')->content}}':</h5>
            </div>
            <form method="post" action="{{route('posts.update', ['post' => $post])}}">
                @method('PUT')
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="en"><b>@lang('Английский')</b></label>
                        <input type="text" class="form-control" id="en"
                               name="en"
                               value="{{$post->translate('en')->content}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="fr"><b>@lang('Французский')</b></label>
                        <input type="text" class="form-control" id="fr"
                               name="fr"
                               value="{{$post->translate('fr')->content}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="es"><b>@lang('Испанский')</b></label>
                        <input type="text" class="form-control" id="es"
                               name="es"
                               value="{{$post->translate('es')->content}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="de"><b>@lang('Немецкий')</b></label>
                        <input type="text" class="form-control" id="de"
                               name="de"
                               value="{{$post->translate('de')->content}}">
                        @error('de')
                        <p class="error">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">@lang('Сохранить переводы')</button>
                    <a class="pl-4" href="{{route('posts.index')}}">@lang('Назад')</a>
                </div>
            </form>
        @endif
    </div>
@endsection
